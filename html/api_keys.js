/*
 * NetworkVisualizer (https://bitbucket.org/netburnerinterns/networkvisualizer)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */
 
 /*
  * Add a property 'MAPBOX_API' to 
  * NetworkVisualizer via the global 'nv' object
  * 
  * Replace 'ACCESS_TOKEN' and 'ID' with your own
  */
   
nv.MAPBOX_API = {
    URL: 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?' +
            'access_token={accessToken}',

    ACCESS_TOKEN: 'your-access-token-here',

    ID:  'your-id-here'
};
