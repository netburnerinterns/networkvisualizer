/*
 * NetworkVisualizer (https://bitbucket.org/netburnerinterns/networkvisualizer)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

/*
 * PacketPainter web app
 *
 * Receives IP addresses from NetBurner board via WebSocket
 * and maps them with the NetworkVisualizer Leaflet framework
 *
 * The property pp.SOCKET_URL is set by the NetBurner tools at compile time
 */
  
(function(pp, undefined) {
    
    /* Private variables */
    
    var websocket        = null;
    var request_interval = null;
    
    var addrs_temp       = {};
    var stopped          = true;
    
    /* Constants */
    
    var REQUEST_DELAY    = 1500;
    var AJAX_TIMEOUT     = REQUEST_DELAY;
    
    var GeoipApi = {
        URL:  'http://www.telize.com/geoip/'
    };
    
    /* Public functions */
    
    /*
     * Called when the start button is pressed
     * Opens a WebSocket, sets handlers, and starts address de-queuing  interval
     * The rest of the client-side code is in networkvisualizer.js
     */
    pp.Start = function() {
        if ('WebSocket' in window) {
            if (websocket == null || websocket.readyState == WebSocket.CLOSED) {

                websocket = new WebSocket(pp.SOCKET_URL);
                
                websocket.onopen = WebSocketOnOpen;
                websocket.onclose = WebSocketOnClose;
                
                websocket.onmessage = function(message) { 
                    WebSocketOnMessage(message); 
                }
                
                request_interval = window.setInterval(
                        ProcessNextAddress, REQUEST_DELAY);
                
                stopped = false;
            }
        } else {
            document.getElementById('console').innerHTML = 'WebSocket unsupported';
        }
    };
    
    /*
     * Called when the stop button is pressed.
     * Closes the WebSocket and stops address de-queuing  interval
     */
    pp.Stop = function() {
        if (websocket && websocket.readyState == WebSocket.OPEN) {
            stopped = true;
            websocket.close();
            window.clearInterval(request_interval);
        }
    };
    
    /*
     * Called when the load button is pressed. Clicks the file input field
     */
    pp.Load = function() {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            document.getElementById('map-file-input').click();
        } else {
            document.getElementById('console').innerHTML = 'File in unsupported';
        }
    };
    
    /*
     * On change handler for file input
     */
    pp.HandleFiles = function(files) {
        var file = document.getElementById('map-file-input').files[0];
        
        if (file != null) {
            var r = new FileReader();
            
            r.onload = function(e) {
                pp.Stop();
                
                if(nv.LoadMapData(e.target.result)) {
                    addrs_temp = {};
                    document.getElementById('map-file-input').value = null;
                } else {
                    document.getElementById('console').innerHTML = 'Invalid file';
                }
            };
            
            r.readAsText(file, document.characterSet);
        }
    };
    
    /*
     * Called when the download button is pressed
     * Saves map data as JSON
     */
    pp.Download = function() {
        PauseProcessing();
        nv.DownloadMapData();
        ResumeProcessing();
    };
    
    /* Private functions */
    
    /*
     * De-queues an ip address and requests its geolocation
     * If request is successful, sets ip data and draws on map
     */
    function ProcessNextAddress() {
        
        for (var ip in addrs_temp) {
            var addr = addrs_temp[ip];
            
            if (!addr.requested) {
            
                // request geolocation of IP address
                
                $.ajax({
                    url: GeoipApi.URL + ip, 
                    type: 'GET', 
                    dataType: 'JSON',
                    timeout: AJAX_TIMEOUT,
                    
                    success: function(data) {
                    
                        // if the reponse is valid,
                        // set ip data and draw on the map
                        
                        if (data.longitude != null) {
                        
                            nv.AddAddr(ip, addr.count_src, addr.count_dst,
                                    data.latitude, data.longitude,
                                    data.country, data.city, data.isp);
                            
                            delete addrs_temp[ip];
                        }
                    },
                });
                
                addr.requested = true;
                break;
            }
        }
    }
    
    /* WebSocket callbacks */

    function WebSocketOnOpen() {
        document.getElementById('console').innerHTML = 'WebSocket connected.';
        document.getElementById('start-button').innerHTML = "Pause";
        document.getElementById('start-button')
                .setAttribute('href', 'javascript:pp.Stop()');
    }
    
    function WebSocketOnClose() {
        document.getElementById('console').innerHTML = 'WebSocket closed.';
        document.getElementById('start-button').innerHTML = "Start";
        document.getElementById('start-button')
                .setAttribute('href', 'javascript:pp.Start()');
    }
    
    function WebSocketOnMessage(message) {

        // get source and destination IP addresses
        // if they've already been processed, update their value and graphics
        // otherwise, add them to the queue 

        var ip_pair = JSON.parse(message.data);
        var src = ip_pair.src;
        var dst = ip_pair.dst;
    
        // source
        
        if (nv.HasAddr(src)) {
        
            var loc_data = nv.GetAddrLoc(src);
            loc_data.IncCountSrc(src);
            
            nv.UpdateLoc(loc_data);

        } else if (addrs_temp[src] != null) {
            
            addrs_temp[src].count_src++;
            
        } else if (src.substring(0, 2) !== '10' 
                && src.substring(0, 3) !== '255' 
                && src.charAt(0)       !== '0') {
    
            addrs_temp[src] = {count_src: 1, count_dst: 0, requested: false};
        }
        
        // destination
    
        if (nv.HasAddr(dst)) {
        
            var loc_data = nv.GetAddrLoc(dst);
            loc_data.IncCountDst(dst);
            
            nv.UpdateLoc(loc_data);
            
        } else if (addrs_temp[dst] != null) {
            
            addrs_temp[dst].count_dst++;
            
        } else if (dst.substring(0, 2) !== '10' 
                && dst.substring(0, 3) !== '255' 
                && dst.charAt(0)       !== '0') {
            
            addrs_temp[dst] = {count_src: 0, count_dst: 1, requested: false};
        }
    }
        
    /* Functions to pause and resume processing queued addresses */

    function PauseProcessing() {
        if (!stopped) {
            window.clearInterval(request_interval);
        }
    }
    
    function ResumeProcessing() {
        if (!stopped) {
            request_interval = window.setInterval(
                    ProcessNextAddress, REQUEST_DELAY);
        }
    }
}
(window.pp = window.pp || {}));
  
/*
 * Called on page load
 * Sets up the map and tries to get user location
 */
window.onload = function() {
    
    nv.StartVisualizer('map', {
        center: [25, 11],
        zoom: 2,
        zoomControl: false,
        attributionControl: false
    });

    /* home location handlers */
    
    var OnClick = function(location) {
        nv.map.off('click', OnClick);
        nv.SetHomeLocation(location.latlng);
        pp.Start();
    }
    
    var OnLocationFound = function(location) {
        nv.map.off('locationfound', OnLocationFound);
        nv.SetHomeLocation(location.latlng);
        pp.Start();
    };
    
    var OnLocationError = function() {
        document.getElementById('console').innerHTML = 
                'Unable to get location. Click to select home location.';
    
        nv.map.off('locationfound', OnLocationFound);
        nv.map.on('click', OnClick);
    }
    
    // try to get user location via browser
    // if location unavailable, have user click to select location
    
    nv.map.on('locationfound', OnLocationFound);
    nv.map.on('locationerror', OnLocationError);
    
    nv.map.locate();
};
