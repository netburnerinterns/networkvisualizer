/*
 * NetworkVisualizer (https://bitbucket.org/netburnerinterns/networkvisualizer)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

/*
 * NetworkVisualizer Leaflet framework
 *
 * Provides functions for visualizing network traffic geo-ip data
 * and for saving/ loading maps. Access via global 'nv' object
 *
 * Call StartVisualizer('map-div') to add map to the window, and
 * AddAddr to add an address to the map
 *
 * Make sure you set the 'MAPBOX_API' property as in api_keys.js 
 *
 * Requires:
 * Leaflet, Leaflet.label, Leaflet.fullscreen, Leaflet.heat, and FileSaver.js
 */

(function(nv, undefined) {

    /* Public properties */

    nv.map = null;

    /* Private variables */
    
    var MAP_FILENAME    = 'ip-map';
    var MAP_FILE_FORMAT = '*/json;charset=utf-8';

    var ip_locs         = {};   // ip str -> loc str
    var locs_data       = {};   // loc str -> LocData
    var show_lines      = true;

    var heat            = null;
    var home_loc        = null;
    var home_circle     = null;

    /* Private data structures */

    /*
     * Represents the data associated with an individual IP address 
     */
    function IpData(isp, count_src, count_dst) {
        this.isp = isp;
        this.count_src = count_src;
        this.count_dst = count_dst;
    }

    /*
     * Represents a location that can hold multiple IP addresses
     * Stores numbers of connections and references its Leaflet FeatureGroup
     */
    function LocData(loc, country, city, count_src, count_dst, addrs_data) {
        this.addrs_data = addrs_data;   // ip str -> IpData
        
        this.loc        = loc;
        this.country    = country;
        this.city       = city;
        
        this.count_src  = count_src;
        this.count_dst  = count_dst;
    
        this.features   = null;
    
        this.IncCountSrc = function(ip) {
            this.count_src++;
            this.addrs_data[ip].count_src++;
        };
        
        this.IncCountDst = function(ip) {
            this.count_dst++;
            this.addrs_data[ip].count_dst++;
        };
    }
    
    /* Public functions */
    
    /*
     * Returns true if this address has been stored
     */
    nv.HasAddr = function(ip) {
        return ip_locs[ip] != null;
    };
    
    /*
     * Returns the LocData location of an IP address
     */
    nv.GetAddrLoc = function(ip) {
        return locs_data[ip_locs[ip]];
    };
    
    /*
     * Adds an address to the map
     *
     * Addresses are stored based on their location - 
     * If the location of this address is already stored, then update it
     * Otherwise, add a new location and draw it for the first time
     */
    nv.AddAddr = function(ip, count_src, count_dst, lat, lng, country, city, isp) {
        
        var loc = GetNormalizedLoc(lat, lng);
        var loc_str = loc.toString();
        var loc_data = locs_data[loc_str];
        var ip_data = new IpData(isp, count_src, count_dst); 
        
        if (loc_data != null) {
            
            // add the ip to the existing location
            
            loc_data.addrs_data[ip] = ip_data;
            loc_data.count_src += count_src;
            loc_data.count_dst += count_dst;
            
            nv.UpdateLoc(loc_data);
            
        } else {
            
            // add a new location for this ip
            
            var addrs_data = {};
            addrs_data[ip] = ip_data;
        
            loc_data = new LocData(loc, country, city, count_src, count_dst, addrs_data);
            
            locs_data[loc_str] = loc_data;
            
            DrawLine(loc_data);
        }
        
        ip_locs[ip] = loc_str;
    };
    
    /*
     * Given a LocData, updates its graphics and heat map
     */
    nv.UpdateLoc = function(loc_data) {
        
        if (loc_data.features != null) {
            
            var color   = GetColor  (loc_data.count_src, loc_data.count_dst);
            var opacity = GetOpacity(loc_data.count_src, loc_data.count_dst);
            var weight  = GetWeight (loc_data.count_src, loc_data.count_dst);
            var text    = GetDescriptionText(loc_data);
            
            loc_data.features.setStyle({
                color: color,
                fillColor: color,
                fillOpacity: opacity,
                opacity: opacity,
                weight: weight
            });
            
            loc_data.features.updateLabelContent(text);
            heat.addLatLng(loc_data.loc);
        }
    };
        
    /*
     * Clears the map and all stored addresses
     */
    nv.ClearAll = function() {
        
        for (var loc in locs_data) {
            var layer = locs_data[loc].features;
            
            if (layer != null) {
                nv.map.removeLayer(layer);
            }
        }
        
        if (home_circle != null) {
            nv.map.removeLayer(home_circle);
        }
        
        heat.setLatLngs([]);
        
        ip_locs = {};
        locs_data = {};
    };
    
    /*
     * Parses data from map file. If the file is valid, 
     * clears addresses and map, loads data from file, and returns true
     */
    nv.LoadMapData = function(data) {
        
        var json = SanitizeJSON(data);
    
        if (json != null && json.hasOwnProperty('home_loc') 
                         && json.hasOwnProperty('locs_data')) {
            nv.ClearAll();
            
            nv.SetHomeLocation(json.home_loc);
            
            for (var i = 0; i < json.locs_data.length; i++) {
                
                var loc_in = json.locs_data[i];
                
                var loc_data = new LocData(
                        L.latLng(loc_in.loc.lat, loc_in.loc.lng),
                        loc_in.country, loc_in.city, loc_in.count_src, 
                        loc_in.count_dst, loc_in.addrs_data);
                
                var loc_str = loc_data.loc.toString();
                
                locs_data[loc_str] = loc_data;
                
                for (var ip in loc_data.addrs_data) {
                    ip_locs[ip] = loc_str;
                }
                
                DrawLine(loc_data);
            }
            
        } else {
            return false;
        }
        
        return true;
    };
    
    /*
     * Serializes home_loc and locs_data and downloads JSON file
     */
    nv.DownloadMapData = function() {
        
        var loc_array = [];
        var i = 0;
        
        for (var loc in locs_data) {
            var loc_data = locs_data[loc];
            
            if (loc_data.loc != null) {
                loc_array[i++] = loc_data;
            }
        }
        
        var json = JSON.stringify({
            home_loc: home_loc,
            locs_data: loc_array
            
        }, function (key, value) {
            if (key === 'features') {
                return undefined;
            }
            return value;
        });
        
        var blob = new Blob(
                [json],
                {type: MAP_FILE_FORMAT}
        );

        var date = new Date();
        var name = MAP_FILENAME + '-' +
                date.toJSON().slice(5, 10) + 't' + 
                date.getHours() + '-' +  
                date.getMinutes() + '-' + 
                date.getSeconds() + '.json';
                
        saveAs(blob, name);
    };
    
    /*
     * Sets the home location, draws it, and centers the map
     */
    nv.SetHomeLocation = function(location) {
        
        home_loc = location;
        
        home_circle = L.circle(home_loc, 50000, {
            color: 'red',
            fillColor: 'red',
            fillOpacity: 0.5
        }).addTo(nv.map);
        
        nv.map.panTo(home_loc);
    };
    
    /*
     * Given a DOM element, sets up the visualizer and adds it to the window
     */
    nv.StartVisualizer = function(map_elem, options) {
        
        nv.map = L.map(map_elem, options);
        
        L.control.fullscreen().addTo(nv.map);
        
        L.tileLayer(     nv.MAPBOX_API.URL, {
            id:          nv.MAPBOX_API.ID,
            accessToken: nv.MAPBOX_API.ACCESS_TOKEN,
        }).addTo(nv.map);
        
        heat = L.heatLayer([], { 
            maxZoom: 18
        }).addTo(nv.map);
        
        L.control.toggleLinesControl().addTo(nv.map);
    };
    
    /* Private functions */
    
    /*
     * Given a LocData, returns a description string
     */
    function GetDescriptionText(loc_data) {

        var text = (loc_data.city    != null ? loc_data.city    + ', ' : '') + 
                   (loc_data.country != null ? loc_data.country + '. ' : '') + 
                    '</br>' +
                    'Incoming: '   + loc_data.count_src + 
                    '. Outgoing: ' + loc_data.count_dst + '.</br></br>';

        for (var ip in loc_data.addrs_data) {
            text += ip + ': ' + loc_data.addrs_data[ip].isp + '</br>';
        }
        
        return text;
    }
    
    /*
     * Given the numbers of source and destination connections, 
     * returns a color for drawing
     */
    function GetColor(count_src, count_dst) {

        var CalcElement = function(count) {
            var e = '';
            
            if (count > 255) {
                e = 'FF';
            } else {
                if (count < 16) {
                    e = '0';
                }
                e += count.toString(16);
            }
            return e;
        };
        
        return '#00' + CalcElement(count_src) + CalcElement(count_dst);
    }
    
    /*
     * Given the numbers of source and destination connections,
     * returns an opacity for drawing
     */
    function GetOpacity(count_src, count_dst) {
        var count = count_src + count_dst;

        if (count > 100) {
            return 1;
        }
        return (count_src + count_dst) / 100;
    }
    
    /*
     * Given the numbers of source and destination connections,
     * returns a weight for drawing
     */
    function GetWeight(count_src, count_dst) {
        if (count_src > 255 || count_dst > 255) {
                return 4;
        }
        return 2;
    }
    
    /*
     * Given a latitude and longitude, returns a leaflet LatLng for drawing
     */
    function GetNormalizedLoc(lat, lng) {
        
        var data_lng = lng;
        var difference = home_loc.lng - data_lng;
        
        // if the location is farther than 180 degrees longitude from home,
        // mirror it to the left or right
                            
        if(Math.abs(difference) >= 180) {
            if (difference <= 0) {
                data_lng -= 360;
            } else {
                data_lng += 360
            }
        }
        
        return L.latLng(lat, data_lng);
    }
    
    /*
     * Draws a line from home_loc to loc_data.loc given properties of loc_data
     * Sets the features property of loc_data
     */
    function DrawLine(loc_data) {
    
        // calculate colors and opacity
        
        var color   = GetColor  (loc_data.count_src, loc_data.count_dst);
        var opacity = GetOpacity(loc_data.count_src, loc_data.count_dst);
        var weight  = GetWeight (loc_data.count_src, loc_data.count_dst);
        var text    = GetDescriptionText(loc_data);
        
        // add the line and the circle to a group, and
        // set the features property of the LocData
        
        var group = L.featureGroup([
        
            L.circle(loc_data.loc, 10000, {
                color: color,
                fillColor: color,
                fillOpacity: opacity
            }),
            
            L.polyline([loc_data.loc, home_loc], {
                color: color,
                fillColor: color,
                fillOpacity: opacity,
                opacity: opacity,
                weight: weight
            })
            
        ]).bindLabel(text);
        
        loc_data.features = group;
        
        // update the map 

        if (show_lines) {
            group.addTo(nv.map);
        }
        
        for (var i = 0; i < loc_data.count_src + loc_data.count_dst; i++) {
            heat.addLatLng(loc_data.loc);
        }
    }
    
    /*
     * Replaces invalid hidden/ special characters in JSON text. Returns object
     *
     * Adapted from code by EdH (http://stackoverflow.com/users/136087):
     * http://stackoverflow.com/questions/14432165/#27725393
     */
    function SanitizeJSON(text) {
        // preserve newlines, etc - use valid JSON
        text = text.replace(/\\n/g, '\\n')  
                   .replace(/\\'/g, "\\'")
                   .replace(/\\"/g, '\\"')
                   .replace(/\\&/g, '\\&')
                   .replace(/\\r/g, '\\r')
                   .replace(/\\t/g, '\\t')
                   .replace(/\\b/g, '\\b')
                   .replace(/\\f/g, '\\f');
        // remove non-printable and other non-valid JSON chars
        text = text.replace(/[\u0000-\u0019]+/g,''); 
        return JSON.parse(text);
    }
    
    /*
     * Leaflet control to toggle line display
     */
        
    L.Control.ToggleLinesControl = L.Control.extend({

        options: {
            position: 'topright',
            title: {
                'true': 'Hide Lines',
                'false': 'Show Lines'
            }
        },
        
        onAdd: function(map) {
            var container = L.DomUtil.create(
                    'div', 'toggle-lines-control leaflet-bar leaflet-control');

            this.link = L.DomUtil.create('a', 
                    'toggle-lines-control-button leaflet-bar-part', container);
            
            this.link.href = '#';
            this._map = map;
            this._toggleTitle();
            
            L.DomEvent.on(this.link, 'click', this._click, this);
            
            return container;
        },
        
        _toggleTitle: function() {
            this.link.title = this.options.title[show_lines];
        },
        
        _click: function(e) {
        
            L.DomEvent.stopPropagation(e);
            L.DomEvent.preventDefault(e);
            
            // show/ hide map lines
            
            if (show_lines) {
                for (var loc in locs_data) {
                    var layer = locs_data[loc].features;
                    if (layer != null) {
                        this._map.removeLayer(layer);
                    }
                }
                L.DomUtil.addClass(this.getContainer(), 
                        'toggle-lines-control-on');
            } else {
                for (var loc in locs_data) {
                    var layer = locs_data[loc].features;
                    if (layer != null) {
                        this._map.addLayer(layer);
                    }
                }
                L.DomUtil.removeClass(
                        this.getContainer(), 'toggle-lines-control-on');
            }
            
            show_lines = !show_lines;
            this._toggleTitle();
        }

    });

    // add control to Leaflet

    L.control.toggleLinesControl = function (options) {
        return new L.Control.ToggleLinesControl(options);
    };
} 
(window.nv = window.nv || {}));
