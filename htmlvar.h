/*
 * NetworkVisualizer (https://bitbucket.org/netburnerinterns/networkvisualizer)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#ifndef HTMLVAR_H_
#define HTMLVAR_H_

#include <tcp.h>

#endif /*HTMLVAR_H_*/
