# __Network Visualizer - PacketPainter__

Source for a network traffic visualizer implemented on NetBurner's MOD5441x platform. It's a hardware packet switch that captures IP addresses and serves a webpage with an interactive map of their geolocations. Plug it into your router and see where your packets are going!  

![Capture4.PNG](https://bitbucket.org/repo/pzXpex/images/4184185095-Capture4.PNG)

*Green indicates more incoming connections; blue indicates more outgoing. 
You can mouse over lines to see IP, location, and ISP data, and mouse over circles to see numbers of connections.*

### Functionality

* Captures packets promiscuously and streams their source and destination IP addresses over a WebSocket
* Serves an HTML page that receives the addresses and requests their geolocations using client-side JavaScript
* Uses the [Leaflet](http://leafletjs.com/) library to draw addresses on a [MapBox](https://www.mapbox.com/) map
* Can save/ load map data for viewing without NetBurner device

### Libraries

* [Leaflet](http://leafletjs.com/)
* [Leaflet.fullscreen](https://github.com/Leaflet/Leaflet.fullscreen)
* [Leaflet.label](https://github.com/Leaflet/Leaflet.label)
* [Leaflet.heat](https://github.com/Leaflet/Leaflet.heat)
* [jQuery](https://jquery.com/)
* [FileSaver](https://github.com/eligrey/FileSaver.js/)

### Notes

Network Visualizer copyright (c) 2015 by  
 
Sam Posner (http://arcadeoftheabsurd.com/)  
and NetBurner, Inc. (http://www.netburner.com/)

Distributed under the BSD 3-Clause License  
See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause