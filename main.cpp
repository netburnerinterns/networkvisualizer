/*
 * NetworkVisualizer (https://bitbucket.org/netburnerinterns/networkvisualizer)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#include "predef.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <startnet.h>
#include <netrx.h>
#include <nettypes.h>
#include <utils.h>
#include <sim.h>
#include <ethervars.h>
#include <init.h>
#include <iosys.h>
#include <netinterface.h>
#include <http.h>
#include <ip.h>
#include <websockets.h>
#include <ethernet.h>

#define WEBSOCKET_URL "IPSTREAM"
#define FIFO_SIZE 1024

extern "C" {
    void UserMain(void* pd);
}

const char* AppName = "Network Traffic Visualizer";

typedef int (*netDoRXFunc)(PoolPtr, WORD, int);
extern http_wshandler* TheWSHandler;

typedef struct {
    void* pUsedByFifo; // used internally by OSFifo
    BOOL free;         // indicates whether this element is free

    IPADDR src;
    IPADDR dst;

} FifoElem;

static DWORD WriteTaskStack[USER_TASK_STK_SIZE];
static FifoElem FifoBuffer[FIFO_SIZE];

static OS_SEM SocketSemaphore;
static OS_FIFO AddrsFifo;

static int socket_fd = -1;
static fd_set write_fds;
static fd_set error_fds;

/*
 * Initializes the array of FIFO structures
 * From NetBurner FIFO example
 */
void InitFifoMembers() {
    for (int i = 0; i < FIFO_SIZE; i++) {
        FifoBuffer[i].free = TRUE;
        FifoBuffer[i].src = 0;
        FifoBuffer[i].dst = 0;
    }
}

/*
 * Returns the index of the first free FIFO structure, or -1
 * From NetBurner FIFO example
 */
int FindFreeFifoStruct() {
    int index = -1;
    int i = 0;

    do {
        if (FifoBuffer[i].free) {
            index = i;
        } else {
            i++;
        }
    } while ((i < FIFO_SIZE) && (index == -1));

    return index;
}

/*
 * Called to process each packet
 * Posts pairs of source and destination IP addresses to FIFO
 */
int myNetDoRX(PoolPtr pp, WORD w, int if_num) {

    EFRAME* eframe = ((EFRAME*)pp->pData);
    InterfaceBlock *pifb = GetInterFaceBlock();

    // ignore non- IP packets and packets destined for this device's mac address

    if ((memcmp(&(eframe->dest_addr), &pifb->theMac, sizeof(&pifb->theMac)) == 0) ||
            (eframe->eType != ETHERNET_ETHERTYPE_IPv4)) {
	    return 0;
    }

    // get a free FIFO struct. if non are available, ignore the packet

    int i = FindFreeFifoStruct();

    if (i < 0) {
        return 0;
    }

    // assign IP addresses to FIFO structure and post

    IPPKT* ip_packet = GetIpPkt(pp);

    FifoBuffer[i].free = FALSE;
    FifoBuffer[i].src = ip_packet->ipSrc;
    FifoBuffer[i].dst = ip_packet->ipDst;

    OSFifoPost(&AddrsFifo, (OS_FIFO_EL*) &FifoBuffer[i]);

    return 0;
}

int MyDoWSUpgrade(HTTP_Request* req, int sock, PSTR url, PSTR rxb) {
    if (httpstricmp(url, WEBSOCKET_URL)) {
        int rv = WSUpgrade(req, sock);

        if (rv >= 0) {
            socket_fd = rv;
            NB::WebSocket::ws_setoption(socket_fd, WS_SO_TEXT);
            OSSemPost(&SocketSemaphore);
            return 2;

        } else {
            return 0;
        }
    }
    NotFoundResponse(sock, url);
    return 0;
}

/*
 * Pends on FIFO; writes IP addresses to WebSocket
 */
void WriteTask(void* pd) {

    while (1) {
        FifoElem* addrs = (FifoElem*) OSFifoPend(&AddrsFifo, 0);

        if (addrs != NULL) {
            if(socket_fd > 0) {

                FD_SET(socket_fd, &write_fds);
                FD_SET(socket_fd, &error_fds);

                select(1, NULL, &write_fds, &error_fds, 0);

                // write to socket
                if (FD_ISSET(socket_fd, &write_fds)) {

                    char src[32];
                    char dst[32];
                    char out[128];

                    snShowIP(src, 32, addrs->src);
                    snShowIP(dst, 32, addrs->dst);

                    siprintf(out,
                            "{\"src\":\"%s\", "
                            "\"dst\":\"%s\"}"
                            , src, dst);

                    writeall(socket_fd, out, strlen(out));

                    NB::WebSocket::ws_flush(socket_fd);
                }

                FD_ZERO(&write_fds);
                FD_ZERO(&error_fds);
            }

            addrs->free = TRUE;
        }
    }
}

void UserMain(void* pd) {
    init();
    EnablePromiscuous();
    StartHTTP();

    iprintf("\nApplication started\n\n");

    // initialize FIFO and start FIFO pend task

    OSFifoInit(&AddrsFifo);
    InitFifoMembers();

    if (OSTaskCreate(WriteTask,
            NULL,
            &WriteTaskStack[USER_TASK_STK_SIZE],
            WriteTaskStack,
            MAIN_PRIO-1)!= OS_NO_ERR) {
        iprintf("Error starting write task\n");
    }

    FD_ZERO(&write_fds);
    FD_ZERO(&error_fds);

    // wait for a WebSocket connection
    OSSemInit(&SocketSemaphore, 0);
    TheWSHandler = MyDoWSUpgrade;

    while (1) {
        OSSemPend(&SocketSemaphore, 0);

        iprintf("Socket Connected\n");

        // start processing packets
        SetCustomNetDoRX(myNetDoRX);

        while(socket_fd > 0) {
            FD_SET(socket_fd, &write_fds);
            FD_SET(socket_fd, &error_fds);

            select(1, NULL, &write_fds, &error_fds, 0);

            // client closed the socket
            if (FD_ISSET(socket_fd, &error_fds)) {

                // stop processing packets
                ClearCustomNetDoRX();

                // close socket
                OSTimeDly(1);

                close(socket_fd);
                socket_fd = -1;

                iprintf("Socket Closed\n");
                break;
            }
        }
        FD_ZERO(&write_fds);
        FD_ZERO(&error_fds);

        OSTimeDly(TICKS_PER_SECOND);
    }
}
